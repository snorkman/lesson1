data class Author(
        val id: Int,
        val firstName: String,
        val secondName: String,
        val country: String
) {
    override fun toString(): String {
        return firstName + " " + secondName
    }
}